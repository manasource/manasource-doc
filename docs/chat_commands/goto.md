# chat commands/goto

## Synopsis

    @goto <charactername>

Warps your character to the location of another character.

## Examples

    @goto "Mr. Foo"

Warps you to the location of the player "Mr. Foo".

## Security

**Exploitable**

This command can be used to give the user an unfair advantage and to go to areas which are not supposed to be accessible to them. It should only be given to selected users.

## See also

* [@where](where.md)
* [@warp](warp.md)
* [@recall](recall.md)
