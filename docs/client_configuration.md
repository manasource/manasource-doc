# client configuration
#  Mana Configuration

**Note:** This article is about setting up your own basic Mana client data, either for testing purposes or because you want to provide an alternative to the official server.

There are several steps to achieve this:

###  Running Mana

 * Create a `data/` folder

 * Prepare the different configuration files as much as you can in it:

**The client requires the following configuration files:** <br />
data/[attributes.xml](attributes.xml.md) <br />
data/[equip.xml](equip.xml.md) <br />
data/[items.xml](items.xml.md) (which is for now also providing hairs, races and playerset definitions for now) <br />
data/[skills.xml](skills.xml.md) <br />
data/[monsters.xml](monsters.xml.md) <br />
data/[npcs.xml](npcs.xml.md) <br />

**The following files are also very important or required to run:** <br />
The [GUI graphics and data](gui_configuration.md), by default in: `graphics/gui` <br />
The tileset graphics, by default in `graphics/tiles` <br />
The maps (\*.tmx files) in `data/[maps/](mapping)` <br />


**The following data is optional but yet visible when lacking:**  <br />

__Graphical data:__ <br />
The playerset sprites. <br />
Hair sprites. <br />
Equipment sprites. <br />
The monster sprites. <br />
The item sprites. <br />
The NPCs sprites <br />
Emotes sprites.

**Note:** For each one of them, see the [sprite system](sprite_system.md) to get to know how to configure sprites animation, and the [particle system](particle_system.md) to adding new particles

__Configuration data:__ <br />
data/[hair.xml](hair.xml.md) <br />
data/[colors.xml](hair.xml.md) <br />
data/[emotes.xml](emotes.xml.md) <br />
data/[skills.xml](skills.xml.md) <br />
data/[specials.xml](specials.xml.md) <br />
data/[status-effects.xml](status-effects.xml.md) <br />

 * You can also get the official client data from [Git](git_repository.md). A basic example is even provided [here](https://github.com/mana/manaserv/tree/master/example).

To start the client when trying your own data, use the given command to avoid auto-update when choosing a server
that is not providing update data yet:

    mana -u -d /path/to/the/data
