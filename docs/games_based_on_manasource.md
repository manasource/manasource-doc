# Games Based on Manasource

Manasource is a platform used by many independent teams to create their online world. While some make use of their freedom to fork and modify the Mana platform to fit their individual needs others use the software in the default state and contribute to its development directly.

## Playable Games

Servers with serious gameplay and administration.

| Name                                                      | Server    | Client                         | Description                                                        |
|-----------------------------------------------------------|-----------|--------------------------------|--------------------------------------------------------------------|
| [The Mana World](https://themanaworld.org)                | tmwAthena | Mana                           | The original Manasource project |
| [Source of Tales](https://sourceoftales.org)              | Manaserv  | ManaMobile fork                | MMORPG based on [Liberated Pixel Cup](https://lpc.opengameart.org/) art |
| [Moubootaur&nbsp;Legends](https://moubootaurlegends.org/) | Hercules  | ManaPlus&nbsp;/&nbsp;Manaverse | A fork of TMW based on Hercules |
| [TMW BR](https://tmw-br.scall.org/)                       | tmwAthena | Mana                           | A fork of TMW in Portuguese |

## Dead Projects

In memory of the projects that once were, and what could have been.

| Name                                                      | Server    | Status             | Description                                                      |
|-----------------------------------------------------------|-----------|--------------------|------------------------------------------------------------------|
| [Invertika](http://invertika.org/)                        | Manaserv  | Testing (2009-12)  | German project with a huge game world                            |
| [Aethyra](http://www.aethyraproject.org/)                 | tmwAthena | Inactive (2009-12) | Fork of The Mana World with laxer quality standards              |
| Whispers of Avalon                                        | Manaserv  | Dead (2012-01)     | A fantasy MMO aiming for a high graphical quality standard       |
| The Alternate World                                       | tmwAthena | Dead (2012-02)     | Fork of The Mana World with laxer quality standards              |
| Mundo Dos Dragões                                         | Manaserv  | Inactive (2009-12) | Brazilian project based on Manasource                            |
| My Path Online                                            | Manaserv  | Dead (2010-03)     | A new fork focusing on the social elements of online gaming      |
| [Evol Online](https://evolonline.org/)                    | Hercules  | Merged into [rEvolt](https://themanaworld.miraheze.org/wiki/Revolt:Main) | Alternative world with high quality standards |

## Server Types

Various server software is used to run the games based on Manasource.

* **tmwAthena** is a fork of the eAthena server software ([tmwAthena](https://git.themanaworld.org/legacy/tmwa))
* **Manaserv** stands for the Mana server software ([Manaserv](https://gitlab.com/manasource/manaserv))
* **Hercules** stands for the Hercules fork of the eAthena server software ([Hercules](https://github.com/HerculesWS/Hercules))
