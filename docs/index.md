#  Welcome to the Mana documentation wiki!

The Manasource project aims at bringing a real innovative, complete and Free 2D-MMORPG generic platform.
With both our server and client provided, you'll be able to set up your own world, or take part of one already
set up by our community.

If you want to get on board, why not try to come on [IRC](irc.md), or try the [forums](http://forums.themanaworld.org).

## Community
 * [Games based on Manasource](games_based_on_manasource.md)
 * [Downloads](http://www.manasource.org/downloads.html.md)
 * [Client Features](client_features.md)
 * [Server Features](server_features.md)

## Administrators documentation

### Mana Client

#### Get it running

 * [Installation of Client](client_installation.md)
 * [Ports](ports.md)
 * [Dependencies](dependencies.md)
 * [Compile Mana from source](compile_mana_from_source.md)
 * [Ports](ports.md)
 * [Dependencies](dependencies.md)
 * [Compile the Mana server from source](compile_manaserv_from_source.md)

#### Basic configuration

 * [Test data](test_data.md)
 * [Client configuration](client_configuration.md)
 * [Update System](update_system.md)
 * [Server configuration](server_configuration.md)
 * [Scripting](scripting.md)
 * [Chat commands](chat_commands.md)
 * [Mapping](mapping.md)
 * [Tools](tools.md)
 * [Client data handling](client_data_handling.md)
 * [Server data handling](server_data_handling.md)
 * [Image dyeing system](image_dyeing_system.md)
 * [The Mana server Protocol](manaserv_protocol.md)
 * [Sprite system](sprite_system.md)
 * [The Mana server Database specifications](database_specifications.md)
 * [Client design overview](client_design_overview.md)
 * [The Mana server statistics system](manaserv_statistics_system.md)

To get support about setting up a **TmwAthena server**, please have a look at [The Mana World wiki](http://wiki.themanaworld.org/index.php/Main_Page).

## Developers documentation

Welcome on board! We're happy that you're thinking about [Joining the project](joining_the_project.md)!

### Found a bug, missing a feature?

 * Report them in our [Bug tracker](bugtracker.md).
 * For what is being worked on, see the [bug tracker](bugtracker.md) or talk with the developers
 * Technical concepts and documentation needing reviews, see [Requests for Comments](requests_for_comments.md)

### Development conventions

 * [Development Process](development_process.md)
 * [Git repository](git_repository.md)
 * [Hacking](hacking.md)
 * [Quality insurance](quality_insurance.md)

### Source code documentation

 * [Doxygen for client](http://manasource.org/doxygen/mana/) ([Doxygen Warnings](http://manasource.org/doxygen/mana-warnings.log))
 * [Doxygen for the Mana server](http://manasource.org/doxygen/manaserv/) ([Doxygen Warnings](http://manasource.org/doxygen/manaserv-warnings.log))
 * Additional Documentation about the eAthena servers (TmwAthena is a modified eAthena server.) can be found on its original [web site](http://www.eathena.ws/).

### External Resources

Be careful! We do not ensure that those are up to date!

 * [The Mana World Wiki](http://wiki.themanaworld.org/index.php/Main_Page) - The Mana World wiki which contains some useful content to play with.
 * [Invertika Wiki](http://wiki.invertika.org/Hauptseite) - Contains information about the Mana client and server in German.

## New Account Registration on this Wiki

This wiki is cloned from a repository:
[https://gitlab.com/manasource/manasource-doc](https://gitlab.com/manasource/manasource-doc)

Pullrequest for fixes and updates are welcome :)
