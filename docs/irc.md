# IRC

The Mana Project operates the following IRC channel: [#mana on irc.libera.chat](https://web.libera.chat/?channels=#mana).

See [The Mana World wiki](https://wiki.themanaworld.org/wiki/The_Mana_World:Chat) for other communication channels.
