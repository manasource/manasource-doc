# Jail Map

A jail map is a map you design without any regular way to enter or leave. It can thus only be entered or left with the help of a [chat command](chat_commands.md) like [@warp](chat_commands/warp.md), [@goto](chat_commands/goto.md) or [@recall](chat_commands/recall.md).

Such a map can have various purposes:

* an environment for testing on a production server without any interference by regular players
* "parking lot" for your own bots
* private meeting place for server personnel
* temporary confinement of characters suspected of breaking server rules, especially illicit bots.
* an alternative to [@ban](chat_commands/ban.md) as a way of suspending abusive players
