# Requests for Comments

Pages needing reviews and comments.

## Concepts for Technical Improvements

### Bertram

* [Playerset layers handling improvement proposal](playerset_handling_improvement.md)

### Crush

* [Pixel-accurate route finding](pixel-accurate_route_finding.md)
* [Runtime map modification](runtime_map_modification.md)
* [permissions.xml](rights.xml.md)
* [global_events.lua](global_events.lua.md)
* [character transfer interface](character_transfer_interface.md)
* [persistent variables for LUA script engine](persistent_variables_for_lua_script_engine.md)
* [Visual manaserv configuration interface](visual_manaserv_configuration_interface.md)
* [Server-sided special handling concept](server-sided_special_handling_concept.md)

### Jaxad0127

* [Client actor hierarchy](client_actor_hierarchy.md)

## Work in Progress

These pages need to be reviewed, completed and fixed in order to officially include them in the manasource wiki documentation.

### ExceptionFault

* [Manaweb Connectors](manaweb_connectors.md)
