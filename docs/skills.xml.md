# skills.xml

The `skills.xml` file is used to set up the different skills used by the player to gain experience while in game.

**Warning: WIP** Currently, the implementation isn't fully tested. Feel free to [report bugs](bugtracker.md).

Here is an example of a [skills.xml](https://github.com/mana/manaserv/blob/master/example/skills.xml) file:

```xml
<?xml version="1.0"?>
<skills>
    <set name="Weapons">
        <skill id="100" name="Unarmed" icon="graphics/images/unarmed.png" default="true" />
        <skill id="101" name="Knife" icon="graphics/images/knife.png" />
        <skill id="102" name="Sword" icon="graphics/images/sword.png" />
    </set>
    <set name="Magic">
        <skill id="200" name="Magic Example" />
    </set>
    <set name="Crafts">
        <skill id="300" name="Craft Example" />
    </set>
</skills>
```

## `skills` node

The root node of the file. Contains a list of skill sets.

### `skill-set` node

A skill set is a group of skills that are visually grouped together in the client's skill window.

| Attribute   | Type        | Required        | Default       | Description                                                                 |
|-------------|-------------|-----------------|---------------|------------------------|
| name        | **string**  | client          | ""            | The skill set name.    |

#### `skill` node

| Attribute   | Type        | Required        | Default       | Description                                                                              |
|-------------|-------------|-----------------|---------------|------------------------------------------------------------------------------------------|
| id          | **integer** | client, server  | 0             | The skill Id used internally. Must be > 0 and unique.                                    |
| name        | **string**  | client, server  | -             | The skill name.                                                                          |
| icon        | **string**  | no              | ""            | The icon image displayed in the client.                                                  |
| default     | **boolean** | server          | false         | Whether this skill is the default one used to get XP. Only one skill can be the default. |
